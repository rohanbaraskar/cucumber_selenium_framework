package stepDefinitions;

import java.util.Hashtable;

import org.openqa.selenium.WebDriver;

import base.BaseTest;
import io.cucumber.java.en.*;
import pageObjects.Login_Page;
import utility.Common_Util;

public class Login_Definition extends BaseTest{
	Login_Page login = new Login_Page(this.getDriver());

	@Given("Initializing data from excel sheet {string} for test case {string} from document {string}")
	public void initializing_data_from_excel_sheet_for_test_case_from_document(String TC_ID, String sheetName, String XL_documentName) {
	    // Write code here that turns the phrase above into concrete actions
	   
		Hashtable<String, String> map = Common_Util.funcGetData(XL_documentName, sheetName, TC_ID);
		System.out.println(map);
		getDriver().get("https://www.amazon.in");
		login.login(map);

	}

	@When("User enters username and password")
	public void user_enters_username_and_password() {
	    // Write code here that turns the phrase above into concrete actions

	}

	@Then("application login should be successful")
	public void application_login_should_be_successful() {
	    // Write code here that turns the phrase above into concrete actions
	  
	}



	
}
