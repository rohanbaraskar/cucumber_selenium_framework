package stepDefinitions;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import base.BaseTest;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber-html-report", "json:target/cucumber.json",
		"junit:target_junit/cucumber.xml" }, features = {
				"src/test/java/features" }, glue = { "stepDefinitions" }, tags = { "@logincheck" })

public class TestRunner extends BaseTest {

	@AfterClass
	public static void quit()

	{
		getDriver().quit();
	}
}
