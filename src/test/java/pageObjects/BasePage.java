package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import base.BaseTest;

public class BasePage extends BaseTest {

	public BasePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		driver = getDriver();
		PageFactory.initElements(driver, this);
	}

}
