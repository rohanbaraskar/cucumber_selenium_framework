
@ApplicationTest
Feature: EndToEnd Test I want to Test my application

  @logincheck
  Scenario Outline: User Login to application
    Given Initializing data from excel sheet '<sheetname>' for test case '<TCID>' from document '<documentname>'
    When User enters username and password
    Then application login should be successful

    Examples: 
      | TCID  | sheetname | documentname |
      | TC_01 | UserData  | TestData     |
@loginfailcheck
  Scenario Outline: User Login to application
    Given Initializing data from excel sheet '<sheetname>' for test case '<TCID>' from document '<documentname>'
    When User enters username and password
    Then application login should be successful

    Examples: 
      | TCID  | sheetname | documentname |
      | TC_01 | UserData  | TestData     |
      